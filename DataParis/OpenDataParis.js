let opendataparisAPI    = "https://opendata.paris.fr/api/records/";
let version             = "1.0/";
let question            = "search/?";
let dataset             = "dataset="+"coronavirus-commercants-parisiens-livraison-a-domicile";
let row                 = "&rows="+2500;
let sort                = "&sort=code_postal";
let facetrequest        = "&facet=";
let faceturl               = facetrequest+"code_postal"+facetrequest+"type_de_commerce";

let request             = opendataparisAPI + version + question + dataset + row + sort + faceturl;
console.log(request);

let reducer = (accumulator, currentValue) => accumulator + currentValue; // Fonction pour additionner les valeurs d'un tableau


let nomArrondissement            = []; // Tableau qui va stocker le nom de l'arrondissement
let nombreDeCommerce             = []; // Tableau qui va stocker la quantité en fonction de l'arrondissement

let TypeCommerce                   = [];
let qteParCommerce                  = [];


LoadData(request)
    .then(function(response){
        console.log(response);

        let facet   = response.facet_groups; // On accède au facet_groups


/******************************************* BAR GRAPH *******************************************/

    for (let j = 0; j < facet[0].facets.length; j++) { // On parcours la 1ere valeur du facet_groups ( ici c'est le tableau avec comme filtre le code postal)
      let nameArrondissement = facet[0].facets[j].name; // On récupère chaque nom d'arrondissement dans le facet
      let countParArrondissement = facet[0].facets[j].count; // On récupère chaque qté par arrondissement ( en fonction de notre filtre toujours)
      
      nomArrondissement.push(nameArrondissement); // On stock chaque fois la valeur dans notre tableau
      nombreDeCommerce.push(countParArrondissement); // On stock chaque fois la valeur dans notre tableau
    }
  
/******************************************* PIE GRAPH FULL TYPE *******************************************/

    for (let j = 0; j < facet[1].facets.length; j++) { // Parcours de notre deuxième tri de données, 2e valeur de notre facet
      let nameType = facet[1].facets[j].name; 
      let countType = facet[1].facets[j].count;

      TypeCommerce.push(nameType);
      qteParCommerce.push(countType);
    }

/******************************************* PIE GRAPH PAR TYPE *******************************************/

let typelabels = ["Alimentation", "Santé & bien-être", "Divertissement & numérique", 
                  "Fleuriste & blanchisserie", "Bricolage", "Autres"];

    let qteCommerceAlim           = []; // Tableau pour stocker la qté des commerces d'alimentation
    let qteCommerceSante          = [];
    let qteCommerceDivertissement = [];
    let qteCommerceFleuriste      = [];
    let qteCommerceBricolage      = [];
    let qteCommerceAutre          = [];

    for (let j = 0; j < facet[1].facets.length; j++) {
      let CatégorieCommerce = facet[1].facets[j].name;
      let countCatégorieCommerce = facet[1].facets[j].count;

  /******************** Alimentation ********************/
      if ( CatégorieCommerce == 'Restaurant ou traiteur'  || 
           CatégorieCommerce == 'Boucherie - charcuterie - rôtisserie' || 
           CatégorieCommerce == 'Boulangerie - pâtisserie' ||
           CatégorieCommerce == 'Primeur' ||
           CatégorieCommerce == 'Alimentation générale et produits de première nécessité' ||
           CatégorieCommerce == 'Fromagerie' ||
           CatégorieCommerce == 'Poissonnerie' ||
           CatégorieCommerce == 'Chocolaterie - Pâtisserie' ||
           CatégorieCommerce == 'Caviste - Brasserie' ||
           CatégorieCommerce == 'Épicerie fine'
           ){
        qteCommerceAlim.push(countCatégorieCommerce);
  /******************** Santé & bien-être ********************/
      } else if (
           CatégorieCommerce == 'Cosmétique'  || 
           CatégorieCommerce == 'Pharmacies et parapharmacies' || 
           CatégorieCommerce == 'Articles médicaux et orthopédiques'
      ){
        qteCommerceSante.push(countCatégorieCommerce);
  /******************** Divertissement & numérique ********************/
      } else if (
           CatégorieCommerce == 'Librairie'  || 
           CatégorieCommerce == 'Artisanat d\'art' || 
           CatégorieCommerce == 'Jouets - Jeux' ||
           CatégorieCommerce == 'Disquaire' ||
           CatégorieCommerce == 'Presse et papeterie' ||
           CatégorieCommerce == 'équipements informatiques' ||
           CatégorieCommerce == 'Matériels de télécommunication'
      ){
        qteCommerceDivertissement.push(countCatégorieCommerce);
  /******************** Fleuriste & blanchisserie ********************/
      } else if (
           CatégorieCommerce == 'Fleuriste' ||
           CatégorieCommerce == 'Blanchisserie-teinturerie'
   ){
     qteCommerceFleuriste.push(countCatégorieCommerce);
  /******************** Bricolage ********************/
      } else if (
      CatégorieCommerce == 'Bricolage'
){
qteCommerceBricolage.push(countCatégorieCommerce);
  /******************** Autres ********************/
      } else if (
  CatégorieCommerce == 'Autre'
){
qteCommerceAutre.push(countCatégorieCommerce);
}
}

    let resultAlim           = [qteCommerceAlim.reduce(reducer)];
    let resultSante          = [qteCommerceSante.reduce(reducer)];
    let resultDivertissement = [qteCommerceDivertissement.reduce(reducer)];
    let resultFleuriste      = [qteCommerceFleuriste.reduce(reducer)];
    let resultBricolage      = [qteCommerceBricolage.reduce(reducer)];
    let resultAutre          = [qteCommerceAutre.reduce(reducer)];

    let resultCommerce = [resultAlim, resultSante, resultDivertissement, resultFleuriste,
        resultBricolage, resultAutre];
    


/******************************************* APPELS DES GRAPHS *******************************************/

BarGraph(   "Commerce", 
            nomArrondissement, 
            "Commerces par arrondisement en livraison ou retrait",
            nombreDeCommerce);

                    
PieGraph(   "Type", 
            TypeCommerce, 
            "Type du commerce en livraison ou retrait",
            qteParCommerce);


PiegraphSpe(  "Specifique", 
              "Type du commerce",
              typelabels, 
              resultCommerce);

    })



    .catch(function(error){
        console.error(error);
    })
