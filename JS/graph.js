/* Fichier JS pour créer mes graph */

function dynamicColors() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
};


function BarGraph(canvasID, labels, title, values) {
    const ctx = document.getElementById(canvasID).getContext('2d');

    const chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: [{
                label: title,
                backgroundColor: 'rgb(26, 115, 232)',
                borderColor: 'black',
                borderWidth: 0.5,
                data: values
            }]
        },

        // Configuration options go here
        options: {
            responsive: true
        }
    });
}



function PieGraph(canvasID, labels, title, values) {
    const ctx = document.getElementById(canvasID).getContext('2d');

    let color = [];
    for (let i = 0; i < labels.length; i++) {
        let rgb = dynamicColors();
        color.push(rgb);
    }

    const chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: [{
                label: title,
                backgroundColor: color,
                borderColor: 'rgb(255)',
                borderWidth: 0.1,
                data: values
            }]
        },

        // Configuration options go here
        options: {
            responsive: true
        }
    });
}




function PiegraphSpe(canvasID, title, labels, values) {
    const ctx = document.getElementById(canvasID).getContext('2d');

    let color = [];
    for (let i = 0; i < labels.length; i++) {
        let rgb = dynamicColors();
        color.push(rgb);
    }

    const chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels:labels,
            datasets: [{
                label: title,
                backgroundColor: color,
                borderColor: 'rgb(255)',
                borderWidth: 0.1,
                data: values
            }]
        },

        // Configuration options go here
        options: {
            responsive: true
        }
    });
}