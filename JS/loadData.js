/*Fichier JS pour charger de manière native des données*/

async function LoadData(request){
    const response  = await fetch(request);
    /**
     * response est l'objet que l'on va recuperé de la méthode fetch. Et on récup ici la requète.
     * Comme c'est une fonction asynchrone et que l'on doit attendre d'avoir reçu la réponse du serveur, on mets la méthode await.
     */
    const rawData   = await response.json();
    /**
     * Comme on peut récupérer différent type de réponse, on attends d'abord avec await avant de manipuler les données.
     * avec le .json, on parse response, en json.
     */

    return rawData;
    /**
     * Comme on va sortir de la fonction, on a besoin d'un retour de résultat, donc on fait un return.
     */
}